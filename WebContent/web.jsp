<%@page import="utilitarios.Hash"%>
<%@ page import="org.json.*"%>
<%@ page import="apiReceiver.HttpRequest"%>
<%@ page import="apiReceiver.LeitorJSON"%>
<%@ page import="apiReceiver.LeitorJSON"%>
<%@ page import="assinatura.*"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.*"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Validador e-Carteirinha</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Roboto">
<link rel="stylesheet" href="assets/css/styles.min.css">
</head>

<body>

	<%
		//String mensagem = request.getParameter("msg");
		//String hash = request.getParameter("hash");
		//String publicKey = request.getParameter("key").replaceAll(" ", "+");
		//boolean validado = ValidadorDSA.validar(Mensagem.estruturarMensagem(mensagem), assinatura, publicKey);
		boolean validado;
		JSONObject dados;
		try{
			validado = request.getAttribute("validade").equals(true);
			dados = Mensagem.getDados(request.getAttribute("matricula").toString());
		}catch(Exception e){
			validado = false;
			dados = null;
		}
		//boolean validado = ValidadorDSA.validarHex(Mensagem.estruturarMensagem(mensagem), "302d02143433d0287fc3e10236da4439fd911b36040c7c28021500acce0c85f6e547730fee3095669ce8f50bbea74e", "308201b83082012c06072a8648ce3804013082011f02818100d7f9ec3036c255c4eac5657d9f7eb67bea24e77eda60be2c5168c8c945ca6de1c92fcdcf7288fcab38a9fea1fa31acebdd9de9144f5ec8418724fb579bd67d3a2cf03a87f8e7d2ded9812f415520bb5b3b6a891e9608609f3d9477bac56f32500309bacf5e998869781ae05cb8c2db6fab5631761b907a2e7b8112e440c6fd15021500c798bb3578d98224e224eba29d1ac96b131d941b02818100c7549d01029119fef867b8f0db52f3e3a68326dc4320101022bb254625a63f18b30b4bb22c4514d033f1738e301c82c8d2a7ff36226bf6d68a8f74b514570b1a33cf7cfe070508ba7643c07425a50098268e8b7e584b296b1739b87da17d1ee9171cd623b6d0d4b048953fd606f0e209c16cb6dac80c5786741a537c2cd8e84603818500028181009fc7d35139643b559beecf850d8e8655aeeb6b824566354e0b4cfa57ebd2e840fddf056f10e326e72d100b3ff3f6b1ef69dd83c5d4b22cc807becaa1674a6c9fa1e8583e5a39aa13efdb0f98a7f80191178740500ff9245ac3a387db0a6fae1dd783c97b282eb25d5099eb6d92252a004f2136c5eac7f909ac97af39d1ccf8df");
		
	%>

	<!-- Cabeçalho  -->
	<div>
		<nav
			class="navbar navbar-light navbar-expand-md bg-info navigation-clean"
			style="margin-bottom: 38px;">
			<div class="container" style="padding-left: 500px;margin-left:150px">
				<a class="navbar-brand text-white font-weight-bold" href="web.jsp"style="font-family: Roboto, sans-serif; font-size: 32px;margin-left:-543px;">
					<img alt="" class="img-fluid w-25 align-self-center"  src="assets/img/Logotipoifro.png" >
				</a>
				<button class="navbar-toggler" data-toggle="collapse"
					data-target="#navcol-1">
					<span class="sr-only">Toggle navigation</span><span
						class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navcol-1">
					<ul class="nav navbar-nav ml-auto"></ul>
				</div>
			</div>
		</nav>
	</div>
	<!-- Fim Cabeçalho  -->

	<!-- Validação  -->
	<%
		if (!validado) {
	%>

	<div
		class="d-flex flex-column justify-content-center align-items-center align-content-center align-self-center mt-auto">
		<img
			class="img-fluid d-flex justify-content-center align-items-center m-auto m-sm-auto m-md-auto m-lg-auto m-xl-auto"
			src="assets/img/x-1152114_1280.png"
			alt="Imagem de Samuel1983 por Pixabay" width="200" height="200"
			style="margin: 24px; margin-top: 52px; margin-right: 19px; margin-bottom: 22px; margin-left: 22px; padding: 6px; padding-top: 0px; padding-bottom: 0px; padding-left: 0px; font-size: 19px;">
		<h1 class="text-center font-weight-bold"
			style="margin-top: 14px; font-family: Roboto, sans-serif;">Documento
			Inválido!</h1>
	</div>
	<p class="text-center"
		style="font-family: Roboto, sans-serif; font-size: 31px;">
		A assinatura digital deste documento não é válida. <br>Procure
		o&nbsp;Departamento de Assistência ao Educando para regularizar sua
		situação!<br> <br>
	</p>

	<%
		} else {
			
	%>

	<div
		class="d-flex flex-column justify-content-center align-items-center align-content-center align-self-center mt-auto">
		<img
			class="img-fluid d-flex justify-content-center align-items-center m-auto m-sm-auto m-md-auto m-lg-auto m-xl-auto"
			src="assets/img/confirmation-1152155_1280.png"
			alt="Imagem de Samuel1983 por Pixabay" width="200" height="200"
			style="margin: 24px; margin-top: 52px; margin-right: 19px; margin-bottom: 22px; margin-left: 22px; padding: 6px; padding-top: 0px; padding-bottom: 0px; padding-left: 0px; font-size: 19px;">
		<h1 class="text-center font-weight-bold"
			style="margin-top: 14px; font-family: Roboto, sans-serif;">Documento
			Válido!</h1>
	</div>
		<div class="row justify-content-center">
		<div class="table-responsive  col-md-4">
          			<table class="table">      
          				<thead>          
          					<tr></tr>          
          				</thead>          
          				<tbody>          
          					<tr>          
          						<td class="font-weight-bold">Nome Completo</td>          
						<td><%= LeitorJSON.getAtributo(dados, "perfil", "nome") %></td>
          					</tr>          
          					<tr>          
          						<td class="font-weight-bold">Data de Nascimento</td>          
						<td><%= LeitorJSON.getAtributo(dados, "perfil", "data_nascimento") %></td>
          					</tr>          
          					<tr>          
          						<td class="font-weight-bold">Matricula</td>          
						<td><%= LeitorJSON.getArrayItem(dados, "matriculas", 0, "numero") %></td>
          					</tr>          
          					<tr>          
          						<td class="font-weight-bold">Curso</td>          
						<td><%= LeitorJSON.getArrayItem(dados, "matriculas", 0, "nomeCurso") %></td>
          					</tr>          
          					<tr>          
          						<td class="font-weight-bold">Campus</td>          
						<td><%= LeitorJSON.getArrayItem(dados, "matriculas", 0, "campus") %></td>
          					</tr>          
          					<tr>          
          						<td class="font-weight-bold">Turno</td>          
						<td><%= LeitorJSON.getArrayItem(dados, "matriculas", 0, "turno") %></td>
          					</tr>          
          				</tbody>          
          			</table>          
		</div>
	</div>

	<%
		}
	%>
	<!-- Fim Validação -->

	<!-- Footer -->
	<div class="footer-dark"
		style="margin-bottom: 0px; margin-top: 219px; height: 70px; padding: 7px;">
		<footer>
			<div class="container">
				<p class="copyright font-weight-bold">Matheus Dias Vieira © 2019</p>
			</div>
		</footer>
	</div>
	<!-- Fim Footer -->





	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
		integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
		integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
		crossorigin="anonymous"></script>
</body>
</html>