package validador;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import assinatura.AssinaturaECDSA;
import assinatura.Mensagem;
@WebServlet("/validar")
public class ValidadorDigitalSignature extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		if(req.getParameter("msg")!=null) {
			String msg = req.getParameter("msg");
			String ass = req.getParameter("ass");
			String key = req.getParameter("key");
			try {
				boolean validado = AssinaturaECDSA.validate(key, ass, Mensagem.estruturarMensagem(msg));
				req.setAttribute("validade", validado);
				req.setAttribute("matricula", msg);
					//resp.sendRedirect("https://www.youtube.com/watch?v=04854XqcfCY");
			} catch (NullPointerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				//req.setAttribute("validade", false);
				//req.setAttribute("matricula", "erro");
			}
			req.getRequestDispatcher("/web.jsp").forward(req, resp);
		}
	}

	
}
