package assinatura;

import org.json.JSONObject;

import apiReceiver.HttpRequest;
import apiReceiver.LeitorJSON;
import utilitarios.Hash;


/** Classe que gera a mensagem de validação a partir da leitura de um json e retorna um Hash SHA256
 *  da mensagem gerada.
 * @author Matheus Dias Vieira
 *
 */
public class Mensagem {
	public static String estruturarMensagem(String mensagem) {
		HttpRequest mHttpRequest = new HttpRequest();
		JSONObject dadosAluno = LeitorJSON.stringToJSONObject(mHttpRequest.conectar(mensagem));
		
		 String nome = (LeitorJSON.getAtributo(dadosAluno, "perfil", "nome")); 
		
		 String cpf = (LeitorJSON.getAtributo(dadosAluno, "perfil", "cpf")); 
		
		 String nomeCurso = (LeitorJSON.getArrayItem(dadosAluno, "matriculas", 0, "nomeCurso")); 
		
		 String campus = (LeitorJSON.getArrayItem(dadosAluno, "matriculas", 0, "campus")); 
		
		 String data_nascimento = (LeitorJSON.getAtributo(dadosAluno, "perfil", "data_nascimento")); 
		
		 String numero = (LeitorJSON.getArrayItem(dadosAluno, "matriculas", 0, "numero")); 
		
		 String turno = (LeitorJSON.getArrayItem(dadosAluno, "matriculas", 0, "turno")); 
		
		 //System.out.print("{nome="+nome+" cpf="+cpf+" nome_curso="+nomeCurso+" campus="+campus+" data_nascimento="+data_nascimento+" matricula="+numero+" turno="+turno+"}");
		return Hash.sha256Digest("{nome="+nome+" cpf="+cpf+" nome_curso="+nomeCurso+" campus="+campus+" data_nascimento="+data_nascimento+" matricula="+numero+" turno="+turno+"}");
	}
	
	public static JSONObject getDados(String matricula) {
		HttpRequest mHttpRequest = new HttpRequest();
		return LeitorJSON.stringToJSONObject(mHttpRequest.conectar(matricula));
	}


}
