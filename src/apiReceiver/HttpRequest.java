package apiReceiver;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


public class HttpRequest {

    public String conectar(String msg) {
    	try {
    		String resultado = null;
    	 URL url = new URL("http://koalazone.ml/api/valid/"+msg);
         HttpURLConnection conexao = (HttpURLConnection) url.openConnection();
         conexao.setRequestMethod("GET");
         if(conexao.getResponseCode() != 200){
             return "Erro ao comunicar-se com o Servidor\nErro: "+conexao.getResponseCode();
             }
         
         BufferedReader leitor = new BufferedReader(new InputStreamReader(conexao.getInputStream()));
         String linha = leitor.readLine();
         while(linha != null){
             resultado = linha+"\n";
             linha = leitor.readLine();
             
         }
         
         conexao.disconnect();
         return resultado;
         
    }catch (Exception e) {
    	System.out.print("ERRRRRRRO");
		return "Erro!";
	}  
    }

  
	
}
